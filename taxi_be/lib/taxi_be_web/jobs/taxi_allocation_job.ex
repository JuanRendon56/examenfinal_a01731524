defmodule TaxiBeWeb.TaxiAllocationJob do
  use GenServer

  def start_link(request, name) do
    GenServer.start_link(__MODULE__, request, name: name)
  end

  #INICIALIZACION
  def init(request) do
    Process.send(self(), :step1, [:nosuspend])
    {:ok, %{request: request}}
  end

  #MANEJO DE INICIACION DE SERVICIOS
  def handle_info(:step1, %{request: request} = state) do

    #Seleccion de taxistas
    task = Task.async(fn -> select_candidate_taxis(request) end)

    compute_ride_fare(request)
    |> notify_customer_ride_fare
    taxis = Task.await(task)

    Process.send(self(), :step2, [:nosuspend])

    {:noreply, state |> Map.put(:candidates, taxis) |> Map.put(:count, 0)}
  end

  #MANEJO DE ENVIO DE PETICIONES
  def handle_info(:step2, %{candidates: taxis, request: request} = state) do
    #Selecciona el primer taxi
    taxiU = hd(taxis)

    #Selecciona el segundo taxi
    taxiD = hd(tl(taxis))

    %{
      "pickup_address" => pickup_address,
      "dropoff_address" => dropoff_address,
      "booking_id" => booking_id
    } = request

    #Request al TaxiUno
    TaxiBeWeb.Endpoint.broadcast(
      "driver:" <> taxiU.nickname,
      "booking_request",
      %{
        msg: "Viaje de '#{pickup_address}' a '#{dropoff_address}'",
        bookingId: booking_id
      })

    #Request al TaxiDos
    TaxiBeWeb.Endpoint.broadcast(
      "driver:" <> taxiD.nickname,
      "booking_request",
      %{
        msg: "Viaje de '#{pickup_address}' a '#{dropoff_address}'",
        bookingId: booking_id
      })

    # Alarma despues de 2 minutos
    timer = Process.send_after(self(), :outoftime, 12000)

    {:noreply, state |> Map.put(:timer, timer)}
  end

  #MANEJO DE TIEMPO
  def handle_info(:outoftime, %{request: request, timer: timer} = state) do
    #Tiempo fuera
      %{"username" => customer} = request
      TaxiBeWeb.Endpoint.broadcast(
        "customer:" <> customer,
        "booking_request",
        %{msg: "Sorry, cannot find a taxi for you"})
        {:noreply, state |> Map.put(timer: nil)}
        Process.cancel_timer(timer)
  end

  #MANEJO DE PETICIONES
  def handle_info(:step3, %{count: count, request: request, timer: timer} = state) do
    #En caso de que AMBOS ya cancelen, se termina.
    if count == 1 do
      %{"username" => customer} = request
      TaxiBeWeb.Endpoint.broadcast(
        "customer:" <> customer,
        "booking_request",
        %{msg: "Sorry, cannot find a taxi for you"})
        {:noreply, state |> Map.put(timer: nil)}
        Process.cancel_timer(timer)
    end
    {:noreply, state |> Map.put(:count, count + 1)}
  end

  #MANEJO DE ACEPTACION
  def handle_info(:step4, %{request: request, driver: driver_username, timer: timer} = state) do
    if timer != nil do
      Process.cancel_timer(timer)
      %{"username" => customer} = request
      TaxiBeWeb.Endpoint.broadcast(
          "customer:" <> customer,
          "booking_request",
          %{msg: "Your taxi will arrive in 3 minutes, your taxi's username is #{driver_username}"})
      {:noreply, state}
    end
  end

  #Cuando cualquier taxista cancela el viaje
  def handle_cast({:driver_rejected}, %{timer: timer} = state) do
    if timer != nil do
      Process.send(self(), :step3, [:nosuspend])
      {:noreply, state}
    end
  end

  #Cuando cualquier taxista acepta
  def handle_cast({:driver_accepted, driver_username}, %{timer: timer} = state) do
    if timer != nil do
      Process.send(self(), :step4, [:nosuspend])
    {:noreply, state |> Map.put(:driver, driver_username) |> Map.put(timer: nil)}
    end
  end

  def compute_ride_fare(request) do
    %{
      "pickup_address" => pickup_address,
      "dropoff_address" => dropoff_address
     } = request

    {:ok, coord1} = TaxiBeWeb.Geolocator.geocode(pickup_address)
    {:ok, coord2} = TaxiBeWeb.Geolocator.geocode(dropoff_address)
    {distance, _duration} = TaxiBeWeb.Geolocator.distance_and_duration(coord1, coord2)
    {request, Float.ceil(distance/100)}
  end

  def notify_customer_ride_fare({request, fare}) do
    %{"username" => customer} = request
   TaxiBeWeb.Endpoint.broadcast("customer:" <> customer, "booking_request", %{msg: "Ride fare: #{fare}"})
  end

  def select_candidate_taxis(%{"pickup_address" => _pickup_address}) do
    [
      %{nickname: "frodo", latitude: 19.0319783, longitude: -98.2349368}, # Angelopolis
      %{nickname: "pipin", latitude: 19.0061167, longitude: -98.2697737}, # Arcangeles
      %{nickname: "merry", latitude: 19.0092933, longitude: -98.2473716} # Paseo Destino
    ]
  end
end
